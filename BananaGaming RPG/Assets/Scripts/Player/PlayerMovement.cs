﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {
	public float runningspeed = 100f;
	public float walkingspeed = 60f;
	public bool canRun = true;
	public static bool canWalk = true;
	public float speed;
	Rigidbody2D rBody;
	Animator anim;
	// Use this for initialization
	void Start () {
		rBody = GetComponent<Rigidbody2D> ();
		anim = GetComponent<Animator> ();
	}

	void Update(){
		if(Input.GetKey ("escape")) {
			Application.Quit();
		}
		if (Input.GetKey(KeyCode.LeftShift)) {
			if(canRun){
			speed = runningspeed;
			}

		}
		if (!Input.GetKey(KeyCode.LeftShift)) {

			speed = walkingspeed;
		}
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (canWalk) {
			Vector2 movement_vector = new Vector2 (Input.GetAxisRaw ("Horizontal"), Input.GetAxisRaw ("Vertical"));
			if (movement_vector != Vector2.zero) {
				anim.SetBool ("isWalking", true);
				anim.SetFloat ("InputX", movement_vector.x);
				anim.SetFloat ("InputY", movement_vector.y);

			} else {
				anim.SetBool ("isWalking", false);
			
			}
			rBody.MovePosition (rBody.position + movement_vector * Time.deltaTime * speed);
		}
	}



}
