﻿using UnityEngine;
using System.Collections;

public class PlayerStats : MonoBehaviour {
	public static int Bananas;
	public static bool hasAnyBananas;
	public static string mainBananaName;
	Animator anim;
	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();

	}
	
	// Update is called once per frame
	void Update () {
		if (Bananas > 0) {
			hasAnyBananas = true;
		}
		if (mainBananaName == "RottenBanana") {
			anim.SetBool ("isRotten", true);		
		}
		if (mainBananaName == "Ripe Banana") {
			anim.SetBool ("isRipe", true);
		}
		if (mainBananaName == "Moderate Banana") {
			anim.SetBool ("isMild", true);
		}
	}
}
