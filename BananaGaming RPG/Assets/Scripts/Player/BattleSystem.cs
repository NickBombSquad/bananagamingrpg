﻿using UnityEngine;
using System.Collections;
public class BattleSystem : MonoBehaviour {
	PlayerMovement move;
	public bool inBattle;
	public string screenlevel = "Menu";
	GameObject[] GrassArray;
	GrassColliderCheck[] grass;
	EnemyClass enemy;
	bool grasschecker;
	public bool hasbattled;
	float randomgrass;
	public bool stopGrassCheck;
	PlayerMovement playermov;
	public bool inGrass;
	SpriteRenderer hitsprite;
	Animator hitAnim;
	public int BattlesFought;
	// Use this for initialization
	void Start () {
		//hitsprite = GameObject.Find ("hit").GetComponent<SpriteRenderer> ();
		enemy = GameObject.Find ("Enemy").GetComponent<EnemyClass> ();
		playermov = GameObject.Find ("Player(Maxim)").GetComponent<PlayerMovement>();
		//hitAnim = GameObject.Find ("hit").GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
		GrassArray = GameObject.FindGameObjectsWithTag ("grass");
		grass = new GrassColliderCheck[GrassArray.Length];
		for (int i = 0; i < GrassArray.Length; i++){
			grass[i] = GrassArray[i].GetComponent<GrassColliderCheck> ();

			//Debug.Log("GotComponent");
		}

		//Debug.Log (inBattle);
		//grass = GameObject.FindGameObjectsWithTag("Grass").GetComponent<GrassColliderCheck> ();
		//move = GameObject.Find ("Player(Maxim)").GetComponent<PlayerMovement> ();
		if (GrassColliderCheck.inGrass) {
			inGrass = true;
			if (!inBattle) {
				if(stopGrassCheck == false){
					if(BattlesFought == 0){
					randomgrass = Mathf.Round(Random.Range (1f,50f));
					}
					if(BattlesFought < 10 && BattlesFought > 1){
						randomgrass = Mathf.Round(Random.Range (1f,100f));
					}
					Debug.Log(randomgrass);
					if(randomgrass == 1f){
						BattlesFought++;
						inBattle = true;
					stopGrassCheck = true;
					}

				}
				if(stopGrassCheck == true){

				}
					

			}
		}

	}
	
	void OnGUI(){
		if (inBattle) {
			PlayerMovement.canWalk = false;
			if (screenlevel == "Menu") {
				if (GUILayout.Button ("Attack")) {
					screenlevel = "AttackScreen";
				}
			}
			if (screenlevel == "AttackScreen") {
				if (GUILayout.Button ("Knife")) {
					screenlevel = "Menu";
					Debug.Log("Knifed");
					enemy.curHp = enemy.curHp - 5f;
					//hitAnim.SetBool ("hit", true);
				}
				else if (GUILayout.Button ("TossBanana")) {
					screenlevel = "Menu";
					Debug.Log("Knifed");
					enemy.curHp = enemy.curHp - 5f;
					//	hitAnim.SetBool ("hit", true);

				}
	
			}

		}
		if (!inBattle) {
			PlayerMovement.canWalk = true;
		}

	}
}
