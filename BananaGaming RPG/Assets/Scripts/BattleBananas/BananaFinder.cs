﻿using UnityEngine;
using System.Collections;

public class BananaFinder : MonoBehaviour {
	Animator anim;
	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
		 
	}
	
	// Update is called once per frame
	void Update () {
		if (PlayerStats.mainBananaName == "RottenBanana") {
			anim.SetBool ("isRotten", true);		
			Debug.Log("ROTTEN");
		}
		if (PlayerStats.mainBananaName == "Ripe Banana") {
			anim.SetBool ("isRipe", true);
		}
		if (PlayerStats.mainBananaName == "Moderate Banana") {
			anim.SetBool ("isMild", true);
		}
	}
}
