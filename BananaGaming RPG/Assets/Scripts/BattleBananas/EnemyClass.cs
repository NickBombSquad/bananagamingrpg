﻿using UnityEngine;
using System.Collections;

public class EnemyClass : MonoBehaviour {
	public string name;
	public float starthp;
	public float curHp;
	public float id = 0f;
	public bool idSet;
	public float minRange = 0f;
	public float maxRange = 1f;


	BattleSystem battle;
	// Use this for initialization
	void Start () {
		battle = GameObject.Find ("BattleAreas").GetComponent<BattleSystem>();
	}
	
	// Update is called once per frame
	void Update () {
		if (!battle.inBattle) {
			idSet = false;
		}
		if (battle.inBattle) {
			if(!idSet){
				ChooseID();
 			
			}
			if(curHp == 0f){
			//	Debug.Log (battle.inBattle);
				battle.stopGrassCheck = false;
				//Debug.Log(id);
				battle.inBattle = false;

			}
			if(curHp < 0f){ 
			//	Debug.Log (battle.inBattle);

				//Debug.Log(id);
				battle.inBattle = false;
				battle.stopGrassCheck = false;

			}


		
	}
	
}



	void ChooseID(){
		id = Mathf.Round(Random.Range (minRange, maxRange));
		Debug.Log("ID has Been Chosen");

		idSet = true;
		SetPlayerStats ();

	}

	void SetPlayerStats(){
		if (id == 0f) {
			name = "BaseWatermelon";
			Debug.Log(id);
			starthp = 5f;
			curHp = 5f;

		}
		if (id == 1f) {
			name = "BaseWatermelon";
			Debug.Log(id);
			starthp = 2f;
			curHp = 2f;

		}
}
}