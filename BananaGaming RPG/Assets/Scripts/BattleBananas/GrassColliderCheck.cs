﻿using UnityEngine;
using System.Collections;

public class GrassColliderCheck : MonoBehaviour {
	public static bool inGrass;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnTriggerEnter2D(Collider2D coll){
		if (coll.gameObject.tag == "Player") {
			inGrass = true;
		}

	}
	void OnTriggerExit2D(Collider2D coll) {
		if (coll.gameObject.tag == "Player") {
			inGrass = false;
		}
	}
}
