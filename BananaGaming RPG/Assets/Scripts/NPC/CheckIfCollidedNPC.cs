﻿using UnityEngine;
using System.Collections;

public class CheckIfCollidedNPC : MonoBehaviour {
	bool firsttime = true;
	bool collidingwithNPC;
	bool talking = false;
	public GameObject house;
	bool TimetoStart;
	int WhereWeAt = 1;
	private string myText = "Rubio: Hello Maxim";
	// Use this for initialization
	void Start () {
		house =  GameObject.Find("StartPoint") ;

	}
	
	// Update is called once per frame
	void Update () {
		if (!TimetoStart) {
			if(collidingwithNPC){
				if(Input.GetButtonDown("E")){
					TimetoStart = true;
				}
			}
		}

		if (TimetoStart) {
			if (Input.GetButtonDown ("E")) {
				if(firsttime){
				WhereWeAt++;
				}
				if(!firsttime){
					TimetoStart = false;
				}

			}
		}
		if (talking) {
			PlayerMovement.canWalk = false;
		}
		if (!talking) {
			PlayerMovement.canWalk = true;
		}
	}

	void OnTriggerEnter2D(Collider2D coll){

		if (coll.gameObject.tag == "Player" )  {
			collidingwithNPC = true;
		}


	}
	void OnGUI(){
		GUI.skin.button.wordWrap = true;
		if (TimetoStart == true) {
			if (firsttime) {
				if (WhereWeAt == 1) {
					myText = GUI.TextField (new Rect (150, 220, 350, 40), myText);
					talking = true;
				}
				if (WhereWeAt == 2) {
					myText = "Rubio: Beware Maxim There are many dangerous creatures";
					myText = GUI.TextField (new Rect (150, 220, 350, 40), myText);
				}
				if (WhereWeAt == 5) {
					talking = false;
					firsttime = false;
					myText = "Rubio: You Should Bring Some Bananas With You";
					myText = GUI.TextField (new Rect (150, 220, 350, 40), myText);
				}
				if (WhereWeAt == 3) {
					myText = "Rubio: including . . .";
					myText = GUI.TextField (new Rect (150, 220, 350, 40), myText);

				}
				if (WhereWeAt == 4) {
					myText = "Rubio: WATERMELONS!!!!!!!!";
					myText = GUI.TextField (new Rect (150, 220, 350, 40), myText);

				}


			

			}
			if(!firsttime){
				if(WhereWeAt > 5){
				if(PlayerStats.Bananas > 0){
					myText = "Rubio: Good Luck Maxim Beware the WaterMelons";
					myText = GUI.TextField (new Rect (150, 220, 350, 40), myText);

				}


				if(PlayerStats.Bananas < 1){
					myText = "Rubio: Go Back Maxim Get Those Bananas";
					myText = GUI.TextField (new Rect (150, 220, 350, 40), myText);
					
				
				}
			}
			}
		}


	}
	}

