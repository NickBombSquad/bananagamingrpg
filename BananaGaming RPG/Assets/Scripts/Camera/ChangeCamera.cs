﻿using UnityEngine;
using System.Collections;

public class ChangeCamera : MonoBehaviour {
	BattleSystem battle;
	public Camera mainCamera;
	public Camera altCamera;
	// Use this for initialization
	void Start () {
		battle = GameObject.Find("BattleAreas").GetComponent<BattleSystem>();
		mainCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
		altCamera = GameObject.Find("BattleCamera").GetComponent<Camera>();
		altCamera.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(battle.inBattle){
			mainCamera.enabled = false;
			altCamera.enabled = true;
		}
		else if(!battle.inBattle){
			altCamera.enabled = false;
			mainCamera.enabled = true;

		}
	
	}
}
